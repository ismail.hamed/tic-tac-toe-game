const { defineConfig } = require("@vue/cli-service");
module.exports = defineConfig({
  transpileDependencies: true,
  css: {
    loaderOptions: {
      scss: {
        additionalData: `
          @import "@/scss/abstracts/_variables.scss";
          @import "@/scss/abstracts/_mixins.scss";
          @import "@/scss/abstracts/_functions.scss";
          @import "@/scss/_medias.scss";
          @import "@/scss/base/_fonts.scss";
          @import "@/scss/base/_breakpoints.scss";
          @import "@/scss/base/_typography.scss";
          @import "@/scss/themes/_theme.scss";  
          `,
      },
    },
  },
});
