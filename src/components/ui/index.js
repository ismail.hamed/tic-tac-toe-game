const requireComponent = require.context(
  // The relative path of the components folder
  "./",
  // Whether or not to look in subfolders
  true,
  // The regular expression used to match base component filenames
  /Base[A-Z]\w+\.(vue|js)$/
);

const register = (app) => {
  requireComponent.keys().forEach((fileName) => {
    const componentConfig = requireComponent(fileName);

    const componentName = fileName
      .split("/")
      .pop()
      ?.replace(/\.\w+$/, "");

    app.component(componentName, componentConfig.default || componentConfig);
  });
};

export default {
  register,
};
