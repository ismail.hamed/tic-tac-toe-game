import { createApp } from "vue";
import App from "./App.vue";
import "@/scss/style.scss";

import BaseComponents from "@/components/ui/index";
const app = createApp(App);
BaseComponents.register(app);
app.mount("#app");
