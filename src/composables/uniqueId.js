import { ref, onMounted } from "vue";
// by convention, composable function names start with "use"
export function useUniqueId() {
  const uniqueId = ref(0);
  // a composable can update its managed state over time.
  function getRandomInt() {
    const min = 1;
    const max = 1000000000;
    return Math.floor(Math.random() * (max - min + 1) + min);
  }
  onMounted(() => (uniqueId.value = getRandomInt()));

  // expose managed state as return value
  return { uniqueId };
}
